# -*- coding: utf-8 -*-
'''
    Keyboard hooking
    
    Add your function to handlers list. It should take one argument (the event itself).
    Then call listen(), which will run in a background thread.
    
    Dominic Canare <dom@greenlightgo.org>
'''
import time
from threading import Thread
import pythoncom, pyHook, win32api

handlers = []
 
def listen():
    t = Thread(target=_listen)
    t.start()

def stop():
    win32api.PostQuitMessage()

def OnKeyboardEvent(event):
    for handler in handlers:
        handler(event)
 
    # return True to pass the event to other handlers/apps
    return True

def _listen():
    hm = pyHook.HookManager()
    hm.KeyDown = OnKeyboardEvent
    hm.HookKeyboard()
    pythoncom.PumpMessages()

# Example
if __name__ == '__main__':
	def debugHandler(event):
		print 'MessageName:',event.MessageName
		print 'Message:',event.Message
		print 'Time:',event.Time
		print 'Window:',event.Window
		print 'WindowName:',event.WindowName
		print 'Ascii:', event.Ascii, chr(event.Ascii)
		print 'Key:', event.Key
		print 'KeyID:', event.KeyID
		print 'ScanCode:', event.ScanCode
		print 'Extended:', event.Extended
		print 'Injected:', event.Injected
		print 'Alt', event.Alt
		print 'Transition', event.Transition
		print '---'
		
	handlers.append(debugHandler)
	listen()
