import socket, threading

class TCPBroadcaster(object):
	def __init__(self, port=1234):
		self.port = port
		self.clients = []
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		# Bind the socket to the port
		server_address = ('localhost', self.port)
		self.socket.bind(server_address)
		
	def start(self):
		self.thread = threading.Thread(target=self.acceptConnections)
		self.thread.start()
		
	def stop(self):
		self.listening = False
		
	def acceptConnections(self):
		self.listening = True
		self.socket.listen(1)
		
		while self.listening:
			(clientsocket, address) = self.socket.accept()
			self.clients.append(TCPBroadcastClient(clientsocket))
			
	def send(self, data):
		for client in self.clients:
			if client.isActive():
				client.send("%s\n" % data)
			else:
				self.clients.remove(client)
		
class TCPBroadcastClient(object):
	def __init__(self, socket=None, host=None, port=1234):
		if socket == None:
			socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			socket.connect((host, port))
		
		self.lastMessage = None

		self.socket = socket
		self.thread = threading.Thread(target=self.listen)
		self.thread.start()
		
	def send(self, data):
		if data != self.lastMessage:
			self.socket.sendall(data)
			self.lastMessage = data
		
	def isActive(self):
		return self.active
		
	def listen(self):
		self.active = True
		try:
			while True:
				data = self.socket.recv(1024)
				if data == 'q':
					break
		except:
			pass
		finally:
			self.socket.close()
			self.active = False
		
		
if __name__ == '__main__':
	import time
	
	bc = TCPBroadcaster()
	bc.start()
	time.sleep(1)
	for i in range(100):
		print "Sending %d" % i
		bc.send("%d" % i)
		time.sleep(1)
