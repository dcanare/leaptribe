# -*- coding: utf-8 -*-
'''
    An EyeTribe-controlled mouse cursor
    
    Dominic Canare <dom@greenlightgo.org>
'''
import time

from pymouse import PyMouse

import pytribe
from selection_detector import *

import keyboard_hook

frameDelay = 0.03
cursorSampleSize = 0

mouse = PyMouse()
selectionDetector = DwellSelect(.5, 25)

killKeyDetected = False
def keyListener(event):
    global killKeyDetected

    if event.Key == "F8" and event.Alt > 0:
        killKeyDetected = True
        keyboard_hook.stop()
    
keyboard_hook.handlers.append(keyListener)
keyboard_hook.listen()

movementCount = 0
while not killKeyDetected:
    currentTime = time.time()
    
    data = pytribe.query_tracker()
    if data is not None and data['values']['frame']['state'] != 4:
        point = data['values']['frame']['avg']
        point = Point(
            int(point['x']),
            int(point['y']),
            0,
            currentTime,
            point
        )
        
        selectionDetector.addPoint(point)
        movementCount += 1
        if movementCount >= cursorSampleSize:
            mouse.move(point.x, point.y)
            movementCount = 0

        if selectionDetector.selection != None:
            selection = selectionDetector.clearSelection()
            print "CLICK", selection
            mouse.click(selection.x, selection.y)
        
    time.sleep(frameDelay)
